const app = Vue.createApp({
data(){
    return{
        playerHealth : 100,
        monsterHealth : 100,
        specialAttackCounter : 0,
        healCounter:0,
        surrnederFlag:null,
        winner : null
      }
},
methods:{
    attackMonster(){
        const attackValueMonster = Math.floor(Math.random()*15)
        if(this.monsterHealth <= 0){
          this.monsterHealth == 0;
        }
        else
        {
          this.monsterHealth = this.monsterHealth - attackValueMonster
        }
        this.attackPlayer()
        this.specialAttackCounter+=1
        this.healCounter+=1
        console.log(this.specialAttackCounter)
    },
    attackPlayer(){
        const attackValuePlayer = Math.floor(Math.random()*20)

        if(this.playerHealth <= 0){
          this.playerHealth == 0;
        }else{
          this.playerHealth = this.playerHealth - attackValuePlayer
        }
        console.log(this.playerHealth)
    },
    specialAttack(){
        const specialAttackValue = Math.floor(Math.random()*25+10)
        this.monsterHealth = this.monsterHealth - specialAttackValue
        this.attackPlayer()
        this.specialAttackCounter+=1
    },
    heal(){
        this.healCounter+=1 
        const healMe = Math.floor(Math.random()*5+20)
        if(this.playerHealth + healMe >=100){
           this.playerHealth = 100;
        }else{
            this.playerHealth+=healMe
        }
        this.attackPlayer()
    },
    newGame(){
        this.playerHealth = 100
        this.monsterHealth = 100
        this.specialAttackCounter = 0
        this.healCounter = 0
        this.winner = null
    },
    surrneder(){
      this.playerHealth = 0;
      this.surrneder = true;

    }
},
computed :{
    //this is coumouted method so its in computed
    playerHealthCalculate(){
       return this.playerHealth+'%'
    },
    monsterHealthCalculate(){
       return this.monsterHealth+'%'
    }

},
watch: {
    playerHealth(value) {
      if (value <= 0 && this.monsterHealth <= 0) {
        // A draw
        this.winner = 'draw';
      } else if (value <= 0) {
        // Player lost
        this.winner = 'monster';
      }
    },
    monsterHealth(value) {
      if (value <= 0 && this.playerHealth <= 0) {
        // A draw
        this.winner = 'draw';
      } else if (value <= 0) {
        // Monster lost
        this.winner = 'player';
      }
    }
}
}).mount('#game')




